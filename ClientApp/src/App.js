import React, {Component} from 'react';
import {Route} from 'react-router';
import {Layout} from './components/Layout';
import {Home} from './components/Home';
import {Position} from './components/Positions/Position'
import {AddPosition} from './components/Positions/AddPosition'
import {EditPosition} from './components/Positions/EditPosition'
import {Employee} from './components/Employee/Employee'
import {AddEmployee} from './components/Employee/AddEmployee'
import {EditEmployee} from './components/Employee/EditEmployee'
import {Client} from './components/Client/Client'
import {EditClient} from './components/Client/EditClient'
import {ClubCard} from './components/ClubCard/ClubCard'
import {EditClubCard} from './components/ClubCard/EditClubCard'
import {ContractServices} from './components/ContractSevices/ContractServices'
import {AddContractServices} from './components/ContractSevices/AddContractServices'
import {PaymentMethod} from './components/PaymentMethod/PaymentMethod'
import {AddPaymentMethod} from './components/PaymentMethod/AddPaymentMethod'
import {EditPaymentMethod} from './components/PaymentMethod/EditPaymentMethod'
import {Placement} from './components/Placement/Placement'
import {AddPlacement} from './components/Placement/AddPlacement'
import {EditPlacement} from './components/Placement/EditPlacement'
import {TypeCard} from './components/TypeCard/TypeCard'
import {AddTypeCard} from './components/TypeCard/AddTypeCard'
import {EditTypeCard} from './components/TypeCard/EditTypeCard'
import {TypeOccupation} from './components/TypeOccupation/TypeOccupation'
import {AddTypeOccupation} from './components/TypeOccupation/AddTypeOccupation'
import {EditTypeOccupation} from './components/TypeOccupation/EditTypeOccupation'
import {Trainer} from './components/Trainer/Trainer'
import {AddTrainer} from './components/Trainer/AddTrainer'
import {EditTrainer} from './components/Trainer/EditTrainer'
import {Schedule} from './components/Schedule/Schedule'
import {AddSchedule} from './components/Schedule/AddSchedule'
import {EditSchedule} from './components/Schedule/EditSchedule'
import {Login} from "./components/Login";
import {Header} from "./components/Header";

export default class App extends Component {
    displayName = App.name

    render() {
        let style;
        if (!sessionStorage.getItem('Token')) {
            style = {
                display: 'none',
            }
        }
        return (
            <React.Fragment>
                <Header/>
                <Layout style={{
                    fontFamily: "Yeseva One"
                }}>
                    <Route exact path='/auth/login' component={Login}/>
                    <Route exact path='/' component={Home}/>

                    <Route exact path='/position' component={Position}/>
                    <Route exact path='/position/add' component={AddPosition}/>
                    <Route path='/position/edit/' component={EditPosition}/>

                    <Route exact path='/employee' component={Employee}/>
                    <Route exact path='/employee/add' component={AddEmployee}/>
                    <Route path='/employee/edit' component={EditEmployee}/>

                    <Route exact path='/client' component={Client}/>
                    <Route path='/client/edit' component={EditClient}/>

                    <Route exact path='/clubcard' component={ClubCard}/>
                    <Route path='/clubcard/edit' component={EditClubCard}/>

                    <Route exact path='/contractservices' component={ContractServices}/>
                    <Route exact path='/contractservices/add' component={AddContractServices}/>

                    <Route exact path='/paymentmethods' component={PaymentMethod}/>
                    <Route exact path='/paymentmethods/add' component={AddPaymentMethod}/>
                    <Route path='/paymentmethods/edit' component={EditPaymentMethod}/>

                    <Route exact path='/placement' component={Placement}/>
                    <Route exact path='/placement/add' component={AddPlacement}/>
                    <Route path='/placement/edit' component={EditPlacement}/>

                    <Route exact path='/typecard' component={TypeCard}/>
                    <Route exact path='/typecard/add' component={AddTypeCard}/>
                    <Route path='/typecard/edit' component={EditTypeCard}/>

                    <Route exact path='/typeoccupation' component={TypeOccupation}/>
                    <Route exact path='/typeoccupation/add' component={AddTypeOccupation}/>
                    <Route path='/typeoccupation/edit' component={EditTypeOccupation}/>

                    <Route exact path='/trainer' component={Trainer}/>
                    <Route exact path='/trainer/add' component={AddTrainer}/>
                    <Route path='/trainer/edit' component={EditTrainer}/>

                    <Route exact path='/schedule' component={Schedule}/>
                    <Route exact path='/schedule/add' component={AddSchedule}/>
                    <Route path='/schedule/edit' component={EditSchedule}/>
                </Layout>
            </React.Fragment>

        );
        // }
    }
}
