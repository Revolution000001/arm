import React, {Component} from 'react';

export class EditTypeOccupation extends Component {
    displayName = EditTypeOccupation.name

    constructor(props) {
        super(props);
        this.state = {
            type: "",
            id: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        this.onSubmit = this.onSubmit.bind(this);
        this.onTypeChange = this.onTypeChange.bind(this);

        fetch('api/TypeOccupation/getType/?id='+this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    type: data.name,
                    loading: false
                });
            });
    }

    onTypeChange(e) {
        this.setState({type: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "name": this.state.type
        });
        fetch('api/TypeOccupation/EditType', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/typeoccupation");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {return (
            <div className="fontAll">
                <h1>Тип занятия</h1>
                <div>
                    <div className="nameAction">
                        Редактирование
                    </div>
                    <div className="__form">
                        <form onSubmit={(e) => this.onSubmit(e)}>
                            <p className="__position">
                                <label className="__labelForm">Название</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.type}
                                       onChange={this.onTypeChange}/>
                            </p>
                            <input  className="saveButton" type="submit" value="Добавить"/>
                        </form>
                    </div>
                </div>
            </div>
        );}

    }
}
