import React, {Component} from 'react';

export class AddTypeOccupation extends Component {
    displayName = AddTypeOccupation.name

    constructor(props) {
        super(props);
        this.state = {
            type: "",
            loading: true
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onTypeChange = this.onTypeChange.bind(this);
    }

    onTypeChange(e) {
        this.setState({type: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "name": this.state.type
        });
        fetch('api/TypeOccupation/AddType', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/typeoccupation");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Тип занятия</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.type}
                                           onChange={this.onTypeChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Добавить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
