﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';
import style from '../style/style.css';

export class NavMenu extends Component {
    displayName = NavMenu.name

    render() {
        return (
            <Navbar inverse fixedTop fluid collapseOnSelect style={{width: 300,fontFamily: "Yeseva One" }}>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to={'/'}>ARM</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to={'/contractservices'}>
                            <NavItem>
                                Договоры
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/schedule'}>
                            <NavItem>
                                Расписание
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/clubcard'}>
                            <NavItem>
                                Клубные карты
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/client'}>
                            <NavItem>
                                Клиенты
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/typecard'}>
                            <NavItem>
                                Тип карты
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/trainer'}>
                            <NavItem>
                                Тренеры
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/placement'}>
                            <NavItem>
                                Помещение
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/employee'}>
                            <NavItem>
                                Работники
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/position'}>
                            <NavItem>
                                Должности
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/typeoccupation'}>
                            <NavItem>
                                Тип занятия
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/paymentmethods'}>
                            <NavItem>
                                Способы оплаты
                            </NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
