﻿import React, {Component} from 'react';
import BrowserRouter from "react-router-dom";

export class AddPosition extends Component {
    displayName = AddPosition.name

    constructor(props) {
        super(props);
        this.state = {position: "", loading: true};

        this.onSubmit = this.onSubmit.bind(this);
        this.onPositionChange = this.onPositionChange.bind(this);
    }

    onPositionChange(e) {
        this.setState({position: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let position = this.state.position;
        let data = JSON.stringify({"name": position});
        fetch('api/Position/AddPosition', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/position");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Должности</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.position}
                                           onChange={this.onPositionChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}
