﻿import React, {Component} from 'react';
import {Link} from "react-router-dom";
import style from '../../style/style.css';

export class Position extends Component {
    displayName = Position.name

    constructor(props) {
        super(props);
        this.state = {positions: [], loading: true};

        fetch('api/Position/getPositions',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({positions: data, loading: false});
            });
    }

    static renderPositionsTable(positions) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {positions.map(position =>
                    <tr key={position.id}>
                        <td>{position.id}</td>
                        <td>{position.name}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/position/edit/${position.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                                <button className="buttonDelete" onClick={(e) => this.delete(position.id, e)}>
                                    Удалить
                                </button>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/Position/DeletePosition', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });

        return window.location.href = "/position";
    }

    // static edit(id, e) {
    //     e.preventDefault();
    //     return window.location.href = "/position/edit/" + id + "";
    // }

    // add(e) {
    //     e.preventDefault();
    //     return window.location.href = "/position/add";
    // }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : Position.renderPositionsTable(this.state.positions);
            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Должности</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/position/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
