import React, {Component} from 'react';
import style from '../../style/style.css';

export class EditPosition extends Component {
    displayName = EditPosition.name

    constructor(props) {
        super(props);
        this.state = {position: "", id: "", loading: true};

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        this.onSubmit = this.onSubmit.bind(this);
        this.onPositionChange = this.onPositionChange.bind(this);

        fetch('api/Position/getPosition/?id=' + this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({id: data.id, position: data.name, loading: false});
            });
    }

    onPositionChange(e) {
        this.setState({position: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let position = this.state.position;
        let data = JSON.stringify({"id": this.state.id, "name": position});
        fetch('api/Position/EditPosition', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/position");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Должности</h1>
                    <div>
                        <div className="nameAction">
                            Редактирование
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.position}
                                           onChange={this.onPositionChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
