﻿import React, {Component} from 'react';
import {Link} from "react-router-dom";

export class TypeCard extends Component {
    displayName = TypeCard.name

    constructor(props) {
        super(props);
        this.state = {types: [], loading: true};

        fetch('api/TypeCard/getTypes', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({types: data, loading: false});
            });
    }

    static renderPositionsTable(types) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Тип карты</th>
                    <th>Цена</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {types.map(type =>
                    <tr key={type.id}>
                        <td>{type.id}</td>
                        <td>{type.name}</td>
                        <td>{type.price}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/typecard/edit/${type.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                                <button className="buttonDelete" onClick={(e) => this.delete(type.id, e)}>
                                    Удалить
                                </button>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/TypeCard/DeleteType', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });
        return window.location.href = "/typecard";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : TypeCard.renderPositionsTable(this.state.types);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Тип карты</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/typecard/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
