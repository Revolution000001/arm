import React, { Component } from 'react';

export class EditTypeCard extends Component {
    displayName = EditTypeCard.name

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            price: "",
            dlitZam: "",
            dlitDeist: "",
            description: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        fetch('api/TypeCard/getType/?id='+this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    name: data.name,
                    price: data.price,
                    dlitZam: data.dlitZam,
                    dlitDeist: data.dlitDeist,
                    description: data.description,
                    loading: false
                });
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onPriceChange = this.onPriceChange.bind(this);
        this.onDlitChange = this.onDlitChange.bind(this);
        this.onDeistChange = this.onDeistChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
    }

    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    onPriceChange(e) {
        this.setState({price: e.target.value});
    }

    onDlitChange(e) {
        this.setState({dlitZam: e.target.value});
    }

    onDeistChange(e) {
        this.setState({dlitDeist: e.target.value});
    }

    onDescChange(e) {
        this.setState({description: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "name": this.state.name,
            "price": this.state.price,
            "dlitZam": this.state.dlitZam,
            "dlitDeist": this.state.dlitDeist,
            "description": this.state.description,
        });
        fetch('api/TypeCard/EditType', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/typecard");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {return (
            <div className="fontAll">
                <h1>Тип карты</h1>
                <div>
                    <div className="nameAction">
                        Редактирование
                    </div>
                    <div className="__form">
                        <form onSubmit={(e) => this.onSubmit(e)}>
                            <p className="__position">
                                <label className="__labelForm">Название</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.name}
                                       onChange={this.onNameChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Цена</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.price}
                                       onChange={this.onPriceChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Длительность заморозки(дн.)</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.dlitZam}
                                       onChange={this.onDlitChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Длительность действия(м.)</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.dlitDeist}
                                       onChange={this.onDeistChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Описание</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.description}
                                       onChange={this.onDescChange}/>
                            </p>
                            <input className="saveButton" type="submit" value="Сохранить"/>
                        </form>
                    </div>
                </div>
            </div>
        );}

    }
}
