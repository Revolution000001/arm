import React, {Component} from 'react';

export class AddEmployee extends Component {
    displayName = AddEmployee.name

    constructor(props) {
        super(props);
        this.state = {
            fio: "",
            phone: "",
            login: "",
            password: "",
            position: "",
            positions: [],
            loading: true
        };

        fetch('api/Position/getPositions', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({positions: data, loading: false});
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onFIOChange = this.onFIOChange.bind(this);
        this.onPhoneChange = this.onPhoneChange.bind(this);
        this.onLoginChange = this.onLoginChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onFIOChange(e) {
        this.setState({fio: e.target.value});
    }

    onPhoneChange(e) {
        this.setState({phone: e.target.value});
    }

    onLoginChange(e) {
        this.setState({login: e.target.value});
    }

    onPasswordChange(e) {
        this.setState({password: e.target.value});
    }

    handleChange(e) {
        this.setState({position: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        // alert(this.state.position);
        // let position = this.state.position;
        let data = JSON.stringify({
            "fio": this.state.fio,
            "phone": this.state.phone,
            "positionId": this.state.position,
            "login": this.state.login,
            "password": this.state.password,
        });
        fetch('api/Employee/AddEmployee', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/employee");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Работники</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">ФИО</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.fio}
                                           onChange={this.onFIOChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Телефон</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.phone}
                                           onChange={this.onPhoneChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Должность</label>
                                    <select  required className="form-control" value={this.state.position} onChange={this.handleChange}>
                                        <option style={{display: 'none'}}></option>
                                        {this.state.positions.map(position =>
                                            <option key={position.id} value={position.id}>{position.name}</option>
                                        )}
                                    </select>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Логин</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.login}
                                           onChange={this.onLoginChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Пароль</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.password}
                                           onChange={this.onPasswordChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
