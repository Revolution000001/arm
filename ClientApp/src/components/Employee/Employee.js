﻿import React, { Component } from 'react';
import {Link} from "react-router-dom";

export class Employee extends Component {
    displayName = Employee.name

    constructor(props) {
        super(props);
        this.state = { employees: [], loading: true };

        fetch('api/Employee/getEmployees',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({ employees: data, loading: false });
            });
    }

    static renderPositionsTable(employees) {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Фио</th>
                        <th>Телефон</th>
                        <th>Должность</th>
                        <th>Действие</th>
                    </tr>
                </thead>
                <tbody>
                    {employees.map(employee =>
                        <tr key={employee.id}>
                            <td>{employee.id}</td>
                            <td>{employee.fio}</td>
                            <td>{employee.phone}</td>
                            <td>{employee.position.name}</td>
                            <td>
                                <div className="linkD">
                                    <Link to={`/employee/edit/${employee.id}`}>
                                        <div className="linkEdit">
                                            Редактировать
                                        </div>
                                    </Link>
                                    <button className="buttonDelete" onClick={(e) => this.delete(employee.id, e)}>
                                        Удалить
                                    </button>
                                </div>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/Employee/DeleteEmployee', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });
        return window.location.href = "/employee";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Employee.renderPositionsTable(this.state.employees);

            return (
                <div className="fontAll __indexFormMargin">
                    <div  className="fontHeading">
                        <h1>Работники</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/employee/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );}

    }
}
