﻿import React, {Component} from 'react';
import style from '../../style/style.css';
import {Link} from "react-router-dom";

export class Client extends Component {
    displayName = Client.name

    constructor(props) {
        super(props);
        this.state = {clients: [], loading: true};

        fetch('api/Client/getClients', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({clients: data, loading: false});
            });
    }

    static renderPositionsTable(clients) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Фио</th>
                    <th>Телефон</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {clients.map(client =>
                    <tr key={client.id}>
                        <td>{client.id}</td>
                        <td>{client.fio}</td>
                        <td>{client.phone}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/client/edit/${client.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : Client.renderPositionsTable(this.state.clients);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1>Клиенты</h1>
                    </div>
                    <div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }
    }
}
