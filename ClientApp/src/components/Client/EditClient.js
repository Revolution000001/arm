import React, {Component} from 'react';
import moment from "moment";
import style from '../../style/style.css';

export class EditClient extends Component {
    displayName = EditClient.name

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            fio: "",
            phone: "",
            seriya: "",
            number: "",
            kemVidan: "",
            dataVidachi: "",
            adresProj: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        fetch('api/Client/getClient/?id=' + this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    fio: data.fio,
                    phone: data.phone,
                    seriya: data.seriya,
                    number: data.number,
                    kemVidan: data.kemVidan,
                    dataVidachi: moment(data.dataVidachi).format('YYYY-MM-DD'),
                    adresProj: data.adresProj,
                    loading: false
                });
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onFioChange = this.onFioChange.bind(this);
        this.onSeriyaChange = this.onSeriyaChange.bind(this);
        this.onNumberChange = this.onNumberChange.bind(this);
        this.onVidanChange = this.onVidanChange.bind(this);
        this.onVidachChange = this.onVidachChange.bind(this);
        this.onAdresChange = this.onAdresChange.bind(this);
        this.onPhoneChange = this.onPhoneChange.bind(this);
    }

    onFioChange(e) {
        this.setState({fio: e.target.value});
    }

    onSeriyaChange(e) {
        this.setState({seriya: e.target.value});
    }

    onNumberChange(e) {
        this.setState({number: e.target.value});
    }

    onVidanChange(e) {
        this.setState({kemVidan: e.target.value});
    }

    onVidachChange(e) {
        this.setState({dataVidachi: e.target.value});
    }

    onAdresChange(e) {
        this.setState({adresProj: e.target.value});
    }

    onPhoneChange(e) {
        this.setState({phone: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "fio": this.state.fio,
            "phone": this.state.phone,
            "seriya": this.state.seriya,
            "number": this.state.number,
            "kemVidan": this.state.kemVidan,
            "dataVidachi": this.state.dataVidachi,
            "adresProj": this.state.adresProj,
        });
        fetch('api/Client/EditClient', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/client");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div  className="fontAll">
                    <h1>Клиенты</h1>
                    <div>
                        <div  className="nameAction">
                            Редактирование
                        </div>
                        <div  className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">ФИО</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.fio}
                                           onChange={this.onFioChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Телефон</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.phone}
                                           onChange={this.onPhoneChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Серия</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.seriya}
                                           onChange={this.onSeriyaChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Номер</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.number}
                                           onChange={this.onNumberChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Кем выдан</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.kemVidan}
                                           onChange={this.onVidanChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Дата выдачи</label>
                                    <input type="date"
                                           required
                                           className="form-control"
                                           value={this.state.dataVidachi}
                                           onChange={this.onVidachChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Адрес проживания</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.adresProj}
                                           onChange={this.onAdresChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
