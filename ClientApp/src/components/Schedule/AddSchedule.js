import React, {Component} from 'react';

export class AddSchedule extends Component {
    displayName = AddSchedule.name

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            vrStart: "",
            vrFinish: "",
            typeoccupation: "",
            typeoccupations: [],
            placement: "",
            placements: [],
            trainer: "",
            trainers: [],
            data: "",
            description: "",
            size: "",
            loading: true
        };

        fetch('api/TypeOccupation/getTypes', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({typeoccupations: data, loading: false});
            });

        fetch('api/Placement/getPlacements', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({placements: data, loading: false});
            });

        fetch('api/Trainer/getTrainers', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({trainers: data, loading: false});
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDataChange = this.onDataChange.bind(this);
        this.onStartChange = this.onStartChange.bind(this);
        this.onFinishChange = this.onFinishChange.bind(this);
        this.onSizeChange = this.onSizeChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.trainerChange = this.trainerChange.bind(this);
        this.typeChange = this.typeChange.bind(this);
        this.placementChange = this.placementChange.bind(this);
    }

    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    onDataChange(e) {
        this.setState({data: e.target.value});
    }

    onStartChange(e) {
        this.setState({vrStart: e.target.value});
    }

    onFinishChange(e) {
        this.setState({vrFinish: e.target.value});
    }

    onSizeChange(e) {
        this.setState({size: e.target.value});
    }

    onDescChange(e) {
        this.setState({description: e.target.value});
    }

    trainerChange(e) {
        this.setState({trainer: e.target.value});
    }

    typeChange(e) {
        this.setState({typeoccupation: e.target.value});
    }

    placementChange(e) {
        this.setState({placement: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "name": this.state.name,
            "vrNach": this.state.vrStart,
            "vrOkon": this.state.vrFinish,
            "typeoccupationID": this.state.typeoccupation,
            "placementID": this.state.placement,
            "trainerID": this.state.trainer,
            "data": this.state.data,
            "description": this.state.description,
            "size": this.state.size,
        });
        fetch('api/Schedule/AddSchedule', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/schedule");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Расписание</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.name}
                                           onChange={this.onNameChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Тренер</label>
                                    <select required className="form-control" value={this.state.trainer} onChange={this.trainerChange}>
                                        <option style={{display: 'none'}}></option>
                                        {this.state.trainers.map(trainer =>
                                            <option key={trainer.id} value={trainer.id}>{trainer.fio}</option>
                                        )}
                                    </select>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Тип занятия</label>
                                    <select required className="form-control" value={this.state.typeoccupation} onChange={this.typeChange}>
                                        <option style={{display: 'none'}}></option>
                                        {this.state.typeoccupations.map(type =>
                                            <option key={type.id} value={type.id}>{type.name}</option>
                                        )}
                                    </select>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Помещение</label>
                                    <select required className="form-control" value={this.state.placement} onChange={this.placementChange}>
                                        <option style={{display: 'none'}}></option>
                                        {this.state.placements.map(placement =>
                                            <option key={placement.id} value={placement.id}>{placement.name}</option>
                                        )}
                                    </select>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Дата</label>
                                    <input type="date"
                                           required
                                           className="form-control"
                                           value={this.state.data}
                                           onChange={this.onDataChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Время начала занятия</label>
                                    <input type="time"
                                           required
                                           className="form-control"
                                           value={this.state.vrStart}
                                           onChange={this.onStartChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Время окончания занятия</label>
                                    <input type="time"
                                           required
                                           className="form-control"
                                           value={this.state.vrFinish}
                                           onChange={this.onFinishChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Размер группы</label>
                                    <input type="number"
                                           required
                                           className="form-control"
                                           value={this.state.size}
                                           onChange={this.onSizeChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Описание</label>
                                    <input type="textarea"
                                           required
                                           className="form-control"
                                           value={this.state.description}
                                           onChange={this.onDescChange}/>
                                </p>
                                <input  className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
