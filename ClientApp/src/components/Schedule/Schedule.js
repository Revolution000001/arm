﻿import React, {Component} from 'react';
import moment from "moment";
import {Link} from "react-router-dom";

export class Schedule extends Component {
    displayName = Schedule.name

    constructor(props) {
        super(props);
        this.state = {schedules: [], loading: true};

        fetch('api/Schedule/getSchedules', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({schedules: data, loading: false});
            });
    }

    static renderPositionsTable(schedules) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Время начала</th>
                    <th>Время выхода</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {schedules.map(schedule =>
                    <tr key={schedule.id}>
                        <td>{schedule.id}</td>
                        <td>{schedule.name}</td>
                        <td>{moment(schedule.data).format('DD/MM/YYYY')}</td>
                        <td>{schedule.vrNach}</td>
                        <td>{schedule.vrOkon}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/schedule/edit/${schedule.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : Schedule.renderPositionsTable(this.state.schedules);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Расписание</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/schedule/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
