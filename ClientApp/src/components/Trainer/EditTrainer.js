import React, {Component} from 'react';

export class EditTrainer extends Component {
    displayName = EditTrainer.name

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            fio: "",
            zaslugi: "",
            phone: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        fetch('api/Trainer/getTrainer/?id=' + this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    fio: data.fio,
                    zaslugi: data.zaslugi,
                    phone: data.phone,
                    loading: false
                });
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onFIOChange = this.onFIOChange.bind(this);
        this.onZaslugiChange = this.onZaslugiChange.bind(this);
        this.onPhoneChange = this.onPhoneChange.bind(this);
    }

    onFIOChange(e) {
        this.setState({fio: e.target.value});
    }

    onZaslugiChange(e) {
        this.setState({zaslugi: e.target.value});
    }

    onPhoneChange(e) {
        this.setState({phone: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "fio": this.state.fio,
            "zaslugi": this.state.zaslugi,
            "phone": this.state.phone,
        });
        fetch('api/Trainer/EditTrainer', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/trainer");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Тренер</h1>
                    <div>
                        <div className="nameAction">
                            Редактирование
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">ФИО</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.fio}
                                           onChange={this.onFIOChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Заслуги</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.zaslugi}
                                           onChange={this.onZaslugiChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Телефон</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.phone}
                                           onChange={this.onPhoneChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
