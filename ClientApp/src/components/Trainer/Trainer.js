﻿import React, {Component} from 'react';
import {Link} from "react-router-dom";

export class Trainer extends Component {
    displayName = Trainer.name

    constructor(props) {
        super(props);
        this.state = {trainers: [], loading: true};

        fetch('api/Trainer/getTrainers', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({trainers: data, loading: false});
            });
    }

    static renderPositionsTable(trainers) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Фио</th>
                    <th>Телефон</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {trainers.map(trainer =>
                    <tr key={trainer.id}>
                        <td>{trainer.id}</td>
                        <td>{trainer.fio}</td>
                        <td>{trainer.phone}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/trainer/edit/${trainer.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                                <button className="buttonDelete" onClick={(e) => this.delete(trainer.id, e)}>
                                    Удалить
                                </button>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/Trainer/DeleteTrainer', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });
        return window.location.href = "/trainer";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : Trainer.renderPositionsTable(this.state.trainers);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Тренеры</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/trainer/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
