﻿import React, {Component} from 'react';
import {Link} from "react-router-dom";

export class Placement extends Component {
    displayName = Placement.name

    constructor(props) {
        super(props);
        this.state = {placements: [], loading: true};

        fetch('api/Placement/getPlacements', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({placements: data, loading: false});
            });
    }

    static renderPositionsTable(placements) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Город</th>
                    <th>Улица</th>
                    <th>Дом</th>
                    <th>Корпус</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {placements.map(placement =>
                    <tr key={placement.id}>
                        <td>{placement.id}</td>
                        <td>{placement.name}</td>
                        <td>{placement.city}</td>
                        <td>{placement.street}</td>
                        <td>{placement.house}</td>
                        <td>{placement.korpus}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/placement/edit/${placement.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                                <button className="buttonDelete" onClick={(e) => this.delete(placement.id, e)}>
                                    Удалить
                                </button>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/Placement/DeletePosition', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });
        return window.location.href = "/placement";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : Placement.renderPositionsTable(this.state.placements);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Помещение</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/placement/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
