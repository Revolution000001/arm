import React, { Component } from 'react';

export class EditPlacement extends Component {
    displayName = EditPlacement.name

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            city: "",
            street: "",
            house: "",
            korpus: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        fetch('api/Placement/getPlacement/?id='+this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    name: data.name,
                    city: data.city,
                    street: data.street,
                    house: data.house,
                    korpus: data.korpus,
                    loading: false
                });
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onCityChange = this.onCityChange.bind(this);
        this.onStreetChange = this.onStreetChange.bind(this);
        this.onHouseChange = this.onHouseChange.bind(this);
        this.onKorpusChange = this.onKorpusChange.bind(this);
    }

    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    onCityChange(e) {
        this.setState({city: e.target.value});
    }

    onStreetChange(e) {
        this.setState({street: e.target.value});
    }

    onHouseChange(e) {
        this.setState({house: e.target.value});
    }

    onKorpusChange(e) {
        this.setState({korpus: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "name": this.state.name,
            "city": this.state.city,
            "street": this.state.street,
            "house": this.state.house,
            "korpus": this.state.korpus,
        });
        fetch('api/Placement/EditPlacement', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/placement");
    };

    render() {
        return (
            <div className="fontAll">
                <h1>Помещение</h1>
                <div>
                    <div className="nameAction">
                        Редактирование
                    </div>
                    <div className="__form">
                        <form onSubmit={(e) => this.onSubmit(e)}>
                            <p  className="__position">
                                <label className="__labelForm">Название</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.name}
                                       onChange={this.onNameChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Город</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.city}
                                       onChange={this.onCityChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Улица</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.street}
                                       onChange={this.onStreetChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Дом</label>
                                <input type="text"
                                       required
                                       className="form-control"
                                       value={this.state.house}
                                       onChange={this.onHouseChange}/>
                            </p>
                            <p className="__position">
                                <label className="__labelForm">Корпус</label>
                                <input type="text"
                                       className="form-control"
                                       value={this.state.korpus}
                                       onChange={this.onKorpusChange}/>
                            </p>
                            <input className="saveButton" type="submit" value="Сохранить"/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
