import React, {Component} from 'react';

export class AddContractServices extends Component {
    displayName = AddContractServices.name

    constructor(props) {
        super(props);
        this.state = {
            fio: "",
            phone: "",
            seriya: "",
            number: "",
            kemVidan: "",
            dataVidachi: "",
            adresProj: "",
            paymentMethod: "",
            employee: "",
            placement: "",
            typeCard: "",
            paymentMethods: [],
            employees: [],
            placements: [],
            typeCards: [],
            loading: true
        };

        fetch('api/Employee/getEmployees',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({employees: data, loading: false});
            });

        fetch('api/PaymentMethod/getMethods',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({paymentMethods: data, loading: false});
            });

        fetch('api/Placement/getPlacements',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({placements: data, loading: false});
            });

        fetch('api/TypeCard/getTypes',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({typeCards: data, loading: false});
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.typeCardChange = this.typeCardChange.bind(this);
        this.employeeChange = this.employeeChange.bind(this);
        this.placementChange = this.placementChange.bind(this);
        this.paymentMethodChange = this.paymentMethodChange.bind(this);
        this.onFioChange = this.onFioChange.bind(this);
        this.onSeriyaChange = this.onSeriyaChange.bind(this);
        this.onNumberChange = this.onNumberChange.bind(this);
        this.onVidanChange = this.onVidanChange.bind(this);
        this.onVidachChange = this.onVidachChange.bind(this);
        this.onAdresChange = this.onAdresChange.bind(this);
        this.onPhoneChange = this.onPhoneChange.bind(this);
    }

    typeCardChange(e) {
        this.setState({typeCard: e.target.value});
    }

    employeeChange(e) {
        this.setState({employee: e.target.value});
    }

    placementChange(e) {
        this.setState({placement: e.target.value});
    }

    paymentMethodChange(e) {
        this.setState({paymentMethod: e.target.value});
    }

    onFioChange(e) {
        this.setState({fio: e.target.value});
    }

    onSeriyaChange(e) {
        this.setState({seriya: e.target.value});
    }

    onNumberChange(e) {
        this.setState({number: e.target.value});
    }

    onVidanChange(e) {
        this.setState({kemVidan: e.target.value});
    }

    onVidachChange(e) {
        this.setState({dataVidachi: e.target.value});
    }

    onAdresChange(e) {
        this.setState({adresProj: e.target.value});
    }

    onPhoneChange(e) {
        this.setState({phone: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "fio": this.state.fio,
            "phone": this.state.phone,
            "seriya": this.state.seriya,
            "number": this.state.number,
            "kemVidan": this.state.kemVidan,
            "dataVidachi": this.state.dataVidachi,
            "adresProj": this.state.adresProj,
            "typecardID": this.state.typeCard,
            "employeeID": this.state.employee,
            "placementID": this.state.placement,
            "paymentmethodID": this.state.paymentMethod,
        });
        fetch('api/ContractServices/AddContract', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/contractservices");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Договоры</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <fieldset>
                                    <legend>Клиент</legend>
                                    <p className="__position">
                                        <label className="__labelForm">ФИО</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.fio}
                                               onChange={this.onFioChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Телефон</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.phone}
                                               onChange={this.onPhoneChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Серия</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.seriya}
                                               onChange={this.onSeriyaChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Номер</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.number}
                                               onChange={this.onNumberChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Кем выдан</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.kemVidan}
                                               onChange={this.onVidanChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Дата выдачи</label>
                                        <input type="date"
                                               required
                                               className="form-control"
                                               value={this.state.dataVidachi}
                                               onChange={this.onVidachChange}/>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Адрес проживания</label>
                                        <input type="text"
                                               required
                                               className="form-control"
                                               value={this.state.adresProj}
                                               onChange={this.onAdresChange}/>
                                    </p>
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        Характеристика договора
                                    </legend>
                                    <p className="__position">
                                        <label className="__labelForm">Тип карты</label>
                                        <select required className="form-control" value={this.state.typeCard} onChange={this.typeCardChange}>
                                            <option style={{display: 'none'}}></option>
                                            {this.state.typeCards.map(type =>
                                                <option key={type.id} value={type.id}>{type.name}</option>
                                            )}
                                        </select>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Работник</label>
                                        <select required className="form-control" value={this.state.employee} onChange={this.employeeChange}>
                                            <option style={{display: 'none'}}></option>
                                            {this.state.employees.map(employee =>
                                                <option key={employee.id} value={employee.id}>{employee.fio}</option>
                                            )}
                                        </select>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Помещение</label>
                                        <select required className="form-control" value={this.state.placement} onChange={this.placementChange}>
                                            <option style={{display: 'none'}}></option>
                                            {this.state.placements.map(placement =>
                                                <option key={placement.id}
                                                        value={placement.id}>{placement.name}</option>
                                            )}
                                        </select>
                                    </p>
                                    <p className="__position">
                                        <label className="__labelForm">Метод оплаты</label>
                                        <select required className="form-control" value={this.state.paymentMethod} onChange={this.paymentMethodChange}>
                                            <option style={{display: 'none'}}></option>
                                            {this.state.paymentMethods.map(paymentMethod =>
                                                <option key={paymentMethod.id}
                                                        value={paymentMethod.id}>{paymentMethod.name}</option>
                                            )}
                                        </select>
                                    </p>
                                </fieldset>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
