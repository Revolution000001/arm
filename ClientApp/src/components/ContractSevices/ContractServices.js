import React, {Component} from 'react';
import moment from "moment";
import style from '../../style/style.css';
import {Link} from "react-router-dom";

export class ContractServices extends Component {
    displayName = ContractServices.name

    constructor(props) {
        super(props);
        this.state = {contracts: [], typeCard: [], loading: true};

        fetch('api/ContractServices/getContracts', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({contracts: data, loading: false});
            });
    }

    static renderPositionsTable(contracts) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Дата заключения</th>
                    <th>Сумма</th>
                    <th>Клиент</th>
                    <th>Тип карты</th>
                </tr>
                </thead>
                <tbody>
                {contracts.map(contract =>
                    <tr key={contract.id}>
                        <td>{contract.id}</td>
                        <td>{moment(contract.dataZakl).format('DD/MM/YYYY')}</td>
                        <td>{contract.summa}</td>
                        <td>{contract.client.fio}</td>
                        <td>{contract.typeCard.name}</td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    export(e) {
        e.preventDefault();
        fetch('api/ContractServices/ExportExcel', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.blob())
            .then(blob => URL.createObjectURL(blob))
            .then(url => {
                window.open(url, '_blank');
                URL.revokeObjectURL(url);
            });
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : ContractServices.renderPositionsTable(this.state.contracts);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1>Договоры</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/contractservices/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <br/>
                        <button style={{border: 0}} className="linkAdd" onClick={(e) => this.export(e)}>Export</button>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>

            );
        }

    }
}
