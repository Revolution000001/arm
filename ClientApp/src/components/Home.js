import React, {Component} from 'react';

export class Home extends Component {
    displayName = Home.name

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div>
                    <h1>Страница в разработке</h1>
                </div>
            );
        }

    }
}
