﻿import React, {Component} from 'react';
import {Link} from "react-router-dom";

export class PaymentMethod extends Component {
    displayName = PaymentMethod.name

    constructor(props) {
        super(props);
        this.state = {methods: [], loading: true};

        fetch('api/PaymentMethod/getMethods', {
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({methods: data, loading: false});
            });
    }

    static renderPositionsTable(methods) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {methods.map(method =>
                    <tr key={method.id}>
                        <td>{method.id}</td>
                        <td>{method.name}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/paymentmethods/edit/${method.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                                <button className="buttonDelete" onClick={(e) => this.delete(method.id, e)}>
                                    Удалить
                                </button>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static delete(id, e) {
        fetch('api/PaymentMethod/DeletePayment', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: JSON.stringify({"id": id}),
        }).then(response => {
            console.log(response);
        });
        return window.location.href = "/paymentmethods";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : PaymentMethod.renderPositionsTable(this.state.methods);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Способ оплаты</h1>
                    </div>
                    <div>
                        <div className="linkWidth">
                            <Link to="/paymentmethods/add">
                                <div className="linkAdd">
                                    Добавить
                                </div>
                            </Link>
                        </div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
