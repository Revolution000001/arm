import React, {Component} from 'react';

export class EditPaymentMethod extends Component {
    displayName = EditPaymentMethod.name

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            id: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);

        fetch('api/PaymentMethod/getPayment/?id=' + this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    name: data.name,
                    description: data.description,
                    loading: false
                });
            });
    }

    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    onDescChange(e) {
        this.setState({description: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "name": this.state.name,
            "description": this.state.description,
        });
        fetch('api/PaymentMethod/EditPayment', {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/paymentmethods");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Способ оплаты</h1>
                    <div>
                        <div className="nameAction">
                            Редактирование
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.name}
                                           onChange={this.onNameChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Описание</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.description}
                                           onChange={this.onDescChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
