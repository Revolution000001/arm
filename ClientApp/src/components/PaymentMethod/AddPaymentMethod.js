import React, {Component} from 'react';

export class AddPaymentMethod extends Component {
    displayName = AddPaymentMethod.name

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            loading: true
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
    }

    onNameChange(e) {
        this.setState({name: e.target.value});
    }

    onDescChange(e) {
        this.setState({description: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "name": this.state.name,
            "description": this.state.description,
        });
        fetch('api/PaymentMethod/AddPayment', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/paymentmethods");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Способ оплаты</h1>
                    <div>
                        <div className="nameAction">
                            Добавление
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Название</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.name}
                                           onChange={this.onNameChange}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Описание</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.description}
                                           onChange={this.onDescChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
