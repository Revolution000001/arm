import React, {Component} from 'react';
import moment from "moment";

export class EditClubCard extends Component {
    displayName = EditClubCard.name

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            dateVidach: "",
            dateOkon: "",
            dlitZam: "",
            frozen: "",
            client: "",
            placement: "",
            type: "",
            loading: true
        };

        let path = JSON.stringify(this.props.location.pathname);
        let app = path.split('/');
        this.state.id = app[3].slice(0, -1);

        fetch('api/ClubCard/getCard/?id=' + this.state.id, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    id: data.id,
                    client: data.client.fio,
                    placement: data.placement.name,
                    type: data.typeCard.name,
                    dateVidach: moment(data.dataVidachi).format('YYYY-MM-DD'),
                    dateOkon: moment(data.dateOkon).format('YYYY-MM-DD'),
                    dlitZam: data.dlitZam,
                    frozen: data.dlitZam,
                    loading: false
                });
            });

        this.onSubmit = this.onSubmit.bind(this);
        this.onDlitZamChange = this.onDlitZamChange.bind(this);
    }

    onDlitZamChange(e) {
        this.setState({dlitZam: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            "id": this.state.id,
            "dateOkon": this.state.dateOkon,
            "dlitZam": this.state.dlitZam,
            "frozen": this.state.frozen - this.state.dlitZam,
        });
        fetch('api/ClubCard/EditCard', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
            body: data,
        }).then(response => {
            console.log(response);
        });
        this.props.history.push("/clubcard");
    };

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            return (
                <div className="fontAll">
                    <h1>Клубная карта</h1>
                    <div>
                        <div className="nameAction">
                            Редактирование
                        </div>
                        <div className="__form">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p className="__position">
                                    <label className="__labelForm">Клиент</label>
                                    <input type="text"
                                           disabled
                                           className="form-control"
                                           value={this.state.client}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Помещение</label>
                                    <input type="text"
                                           disabled
                                           className="form-control"
                                           value={this.state.placement}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Тип карты</label>
                                    <input type="text"
                                           className="form-control"
                                           disabled
                                           value={this.state.type}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Дата выдачи</label>
                                    <input type="date"
                                           className="form-control"
                                           disabled
                                           value={this.state.dateVidach}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Дата окончания</label>
                                    <input type="date"
                                           className="form-control"
                                           disabled
                                           value={this.state.dateOkon}/>
                                </p>
                                <p className="__position">
                                    <label className="__labelForm">Длительность заморозки</label>
                                    <input type="text"
                                           required
                                           className="form-control"
                                           value={this.state.dlitZam}
                                           onChange={this.onDlitZamChange}/>
                                </p>
                                <input className="saveButton" type="submit" value="Сохранить"/>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
