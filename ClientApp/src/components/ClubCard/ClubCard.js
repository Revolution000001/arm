﻿import React, {Component} from 'react';
import style from '../../style/style.css';
import {Link} from "react-router-dom";


export class ClubCard extends Component {
    displayName = ClubCard.name

    constructor(props) {
        super(props);
        this.state = {cards: [], loading: true};

        fetch('api/ClubCard/getCards',{
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('Token')
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({cards: data, loading: false});
            });
    }

    static renderPositionsTable(cards) {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Клиент</th>
                    <th>Тип карты</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {cards.map(card =>
                    <tr key={card.id}>
                        <td>{card.id}</td>
                        <td>{card.client.fio}</td>
                        <td>{card.typeCard.name}</td>
                        <td>
                            <div className="linkD">
                                <Link to={`/clubcard/edit/${card.id}`}>
                                    <div className="linkEdit">
                                        Редактировать
                                    </div>
                                </Link>
                            </div>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    static edit(id, e) {
        e.preventDefault();
        return window.location.href = "/clubcard/edit/" + id + "";
    }

    render() {
        if (!sessionStorage.getItem('Token')) {
            return window.location.href = "/auth/login";
        } else {
            let contents = this.state.loading
                ? <p><em>Loading...</em></p>
                : ClubCard.renderPositionsTable(this.state.cards);

            return (
                <div className="fontAll __indexFormMargin">
                    <div className="fontHeading">
                        <h1 >Клубная карта</h1>
                    </div>
                    <div>
                        <div>
                            {contents}
                        </div>
                    </div>
                </div>
            );
        }

    }
}
