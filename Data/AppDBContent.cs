﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;

namespace ARM.Data
{
    public class AppDBContent : DbContext
    {
        public AppDBContent(DbContextOptions<AppDBContent> options) : base(options)
        {

        }

        public DbSet<Client> Client { get; set; }
        public DbSet<ClubCard> ClubCard { get; set; }
        public DbSet<ClubCardSchedule> ClubCardSchedule { get; set; }
        public DbSet<ContractServices> ContractServices { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<Placement> Placement { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Trainer> Trainer { get; set; }
        public DbSet<TypeCard> TypeCard { get; set; }
        public DbSet<TypeOccupation> TypeOccupation { get; set; }
        public DbSet<Visits> Visits { get; set; }
    }
}
