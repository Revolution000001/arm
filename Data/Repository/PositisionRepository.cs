﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class PositisionRepository : IPosition
    {
        private readonly AppDBContent appDBContent;

        public PositisionRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Position> Positions => appDBContent.Position;

        public Position getPosition(int id)
        {
            return appDBContent.Position.FirstOrDefault(x => x.id == id);
        }
    }
}
