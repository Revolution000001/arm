﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class ClubCardRepository : IClubCard
    {
        private readonly AppDBContent appDBContent;

        public ClubCardRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<ClubCard> ClubCards => appDBContent.ClubCard
            .Include(x => x.Client)
            .Include(x => x.TypeCard);

        public ClubCard getClubCard(int id)
        {
            return appDBContent.ClubCard
                .Include(x => x.Client)
                .Include(x => x.TypeCard)
                .Include(x => x.Placement)
                .FirstOrDefault(x => x.id == id);
        }
    }
}
