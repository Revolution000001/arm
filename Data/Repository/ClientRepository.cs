﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class ClientRepository : IClient
    {
        private readonly AppDBContent appDBContent;

        public ClientRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Client> Clients => appDBContent.Client;

        public Client getClient(int id)
        {
            return appDBContent.Client.FirstOrDefault(x => x.id == id);
        }
    }
}
