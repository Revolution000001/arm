﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class PaymentMethodRepository : IPaymentMethod
    {
        private readonly AppDBContent appDBContent;

        public PaymentMethodRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<PaymentMethod> PaymentMethods => appDBContent.PaymentMethod;

        public PaymentMethod getPaymentMethod(int id)
        {
            return appDBContent.PaymentMethod.FirstOrDefault(x => x.id == id);
        }
    }
}
