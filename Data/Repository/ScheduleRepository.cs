﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class ScheduleRepository : ISchedule
    {
        private readonly AppDBContent appDBContent;

        public ScheduleRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Schedule> Schedules => appDBContent.Schedule.Include(x => x.Placement)
            .Include(x => x.Trainer)
            .Include(x => x.TypeOccupation);

        public Schedule getSchedule(int id)
        {
            return appDBContent.Schedule
                .Include(x => x.Trainer)
                .Include(x => x.TypeOccupation)
                .Include(x => x.Placement)
                .FirstOrDefault(x => x.id == id);
        }
    }
}
