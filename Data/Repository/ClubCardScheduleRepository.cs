﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class ClubCardScheduleRepository : IClubCardSchedule
    {
        private readonly AppDBContent appDBContent;

        public ClubCardScheduleRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Models.ClubCardSchedule> ClubCardSchedules => appDBContent.ClubCardSchedule;

        public Models.ClubCardSchedule getClubCardSchedule(int id)
        {
            return appDBContent.ClubCardSchedule.FirstOrDefault(x => x.id == id);
        }
    }
}
