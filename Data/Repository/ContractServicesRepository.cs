﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class ContractServicesRepository : IContractServices
    {
        private readonly AppDBContent appDBContent;

        public ContractServicesRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<ContractServices> ContractServices => appDBContent.ContractServices
            .Include(x => x.Client)
            .Include(x => x.Employee)
            .Include(x => x.PaymentMethod)
            .Include(x => x.TypeCard)
            .Include(x => x.Placement);

        public ContractServices getContract(int id)
        {
            return appDBContent.ContractServices.FirstOrDefault(x => x.id == id);
        }
    }
}
