﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class EmployeeRepository : IEmployee
    {
        private readonly AppDBContent appDBContent;

        public EmployeeRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Employee> Employees => appDBContent.Employee.Include(x => x.Position);

        public Employee getEmployee(int id)
        {
            return appDBContent.Employee.FirstOrDefault(x => x.id == id);
        }
    }
}
