﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class TypeOccupationRepository : ITypeOccupation
    {
        private readonly AppDBContent appDBContent;

        public TypeOccupationRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<TypeOccupation> TypeOccupations => appDBContent.TypeOccupation;

        public TypeOccupation getTypeOccupation(int id)
        {
            return appDBContent.TypeOccupation.FirstOrDefault(x => x.id == id);
        }
    }
}
