﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class PlacementRepository : IPlacement
    {
        private readonly AppDBContent appDBContent;

        public PlacementRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Placement> Placements => appDBContent.Placement;

        public Placement getPlacement(int id)
        {
            return appDBContent.Placement.FirstOrDefault(x => x.id == id);
        }
    }
}
