﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ARM.Data.Repository
{
    public class VisitsRepository : IVisits
    {
        private readonly AppDBContent appDBContent;

        public VisitsRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Visits> Visits => appDBContent.Visits.Include(x => x.ClubCard.Client);

        public Visits getVisit(int id)
        {
            return appDBContent.Visits.FirstOrDefault(x => x.id == id);
        }
    }
}
