﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class TypeCardRepository : ITypeCard
    {
        private readonly AppDBContent appDBContent;

        public TypeCardRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<TypeCard> TypeCards => appDBContent.TypeCard;

        public TypeCard getTypeCard(int id)
        {
            return appDBContent.TypeCard.FirstOrDefault(x => x.id == id);
        }
    }
}
