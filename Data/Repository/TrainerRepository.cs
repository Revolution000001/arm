﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;

namespace ARM.Data.Repository
{
    public class TrainerRepository : ITrainer
    {
        private readonly AppDBContent appDBContent;

        public TrainerRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Trainer> Trainers => appDBContent.Trainer;

        public Trainer getTrainer(int id)
        {
            return appDBContent.Trainer.FirstOrDefault(x => x.id == id);
        }
    }
}
