﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Placement
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(50)]
        public string street { get; set; }

        [StringLength(50)]
        public string house { get; set; }

        [StringLength(50)]
        public string korpus { get; set; }
    }
}
