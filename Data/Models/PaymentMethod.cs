﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Data.Models
{
    public class PaymentMethod
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string name  { get; set; }

        [StringLength(250)]
        public string description { get; set; }
    }
}
