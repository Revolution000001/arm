﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Visits
    {
        public int id { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan vremVh { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan vremVih { get; set; }

        [DataType(DataType.Date)]
        public DateTime data { get; set; }

        public int clubcardID { get; set; }

        public virtual ClubCard ClubCard { get; set; }
    }
}
