﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARM.Data.Models
{
    public class Employee
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string fio { get; set; }

        [StringLength(20)]
        public string phone { get; set; }


        //[ForeignKey("Position")]
        public int positionId { get; set; }

        public Position Position { get; set; }

        [StringLength(50)]
        public string login { get; set; }

        [StringLength(50)]
        public string password { get; set; }
    }
}
