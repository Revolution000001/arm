﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Trainer
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string fio { get; set; }

        [StringLength(2500)]
        public string zaslugi { get; set; }

        [StringLength(15)]
        public string phone { get; set; }
    }
}
