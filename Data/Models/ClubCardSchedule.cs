﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Data.Models
{
    public class ClubCardSchedule
    {
        public int id { get; set; }

        public int clubcardID { get; set; }
        public virtual ClubCard ClubCard { get; set; }

        public int scheduleID { get; set; }
        public virtual Schedule Schedule { get; set; }
    }
}
