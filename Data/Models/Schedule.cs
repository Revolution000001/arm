﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Schedule
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan vrNach { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan vrOkon { get; set; }

        public int typeoccupationID { get; set; }
        public virtual TypeOccupation TypeOccupation { get; set; }

        public int placementID { get; set; }
        public virtual Placement Placement { get; set; }

        public int trainerID { get; set; }
        public virtual Trainer Trainer { get; set; }

        [DataType(DataType.Date)]
        public DateTime data { get; set; }

        [StringLength(250)]
        public string description { get; set; }

        public int size { get; set; }
    }
}
