﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class TypeCard
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        public int price { get; set; }

        public int dlitZam { get; set; }

        public int dlitDeist { get; set; }

        [StringLength(500)]
        public string description { get; set; }
    }
}
