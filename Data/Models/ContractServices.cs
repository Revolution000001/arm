﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class ContractServices
    {
        [Key]
        public int id { get; set; }

        [DataType(DataType.Date)]
        public DateTime dataZakl { get; set; }

        public int summa { get; set; }

        public int employeeID { get; set; }
        public virtual Employee Employee { get; set; }

        public int paymentmethodID { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }

        public int clientID { get; set; }
        public virtual Client Client { get; set; }

        public int clubcardID { get; set; }
        public virtual ClubCard ClubCard { get; set; }

        public int typecardID { get; set; }
        public virtual TypeCard TypeCard { get; set; }

        public int placementID { get; set; }
        public virtual Placement Placement { get; set; }

    }
}
