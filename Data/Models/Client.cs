﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Client
    {
        public int id { get; set; }

        [StringLength(50)]
        public string fio { get; set; }

        public int seriya { get; set; }

        public int number { get; set; }

        [StringLength(20)]
        public string phone { get; set; }

        [StringLength(250)]
        public string kemVidan { get; set; }

        [DataType(DataType.Date)]
        public DateTime dataVidachi { get; set; }

        [StringLength(250)]
        public string adresProj { get; set; }
    }
}
