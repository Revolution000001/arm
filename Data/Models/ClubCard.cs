﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class ClubCard
    {
        [Key]
        public int id { get; set; }
        
        public int clientID { get; set; }
        public virtual Client Client { get; set; }

        public int typecardID { get; set; }
        public virtual TypeCard TypeCard { get; set; }

        public int placementID { get; set; }
        public virtual Placement Placement { get; set; }

        [DataType(DataType.Date)]
        public DateTime dateVidach { get; set; }

        [DataType(DataType.Date)]
        public DateTime dateOkon { get; set; }

        public int dlitZam { get; set; }
    }
}
