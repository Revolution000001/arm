﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARM.Data.Models
{
    public class Position
    {
        [Key]
        public int id { get; set; }

        [StringLength(50)]
        public string name { get; set; }
    }
}
