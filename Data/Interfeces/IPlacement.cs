﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;

namespace ARM.Data.Interfeces
{
    public interface IPlacement
    {
        IEnumerable<Placement> Placements { get; }

        Placement getPlacement(int id);
    }
}
