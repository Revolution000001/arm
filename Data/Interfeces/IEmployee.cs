﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;

namespace ARM.Data.Interfeces
{
    public interface IEmployee
    {
        IEnumerable<Employee> Employees { get; }

        Employee getEmployee(int id);
    }
}
