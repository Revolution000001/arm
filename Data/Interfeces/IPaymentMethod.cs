﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;

namespace ARM.Data.Interfeces
{
    public interface IPaymentMethod
    {
        IEnumerable<PaymentMethod> PaymentMethods { get; }

        PaymentMethod getPaymentMethod(int id);
    }
}
