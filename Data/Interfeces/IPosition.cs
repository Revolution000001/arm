﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;


namespace ARM.Data.Interfeces
{
    public interface IPosition
    {
        IEnumerable<Position> Positions { get; }

        Position getPosition(int id);
    }
}
