﻿using ARM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Data.Interfeces
{
    public interface IClient
    {
        IEnumerable<Client> Clients { get; }

        Client getClient(int id);
    }
}
