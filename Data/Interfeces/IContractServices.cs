﻿using ARM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Data.Interfeces
{
    public interface IContractServices
    {
        IEnumerable<ContractServices> ContractServices { get; }

        ContractServices getContract(int id);
    }
}
