﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Models;

namespace ARM.Data.Interfeces
{
    public interface IVisits
    {
        IEnumerable<Visits> Visits { get; }

        Visits getVisit(int id);
    }
}
