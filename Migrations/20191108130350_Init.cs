﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ARM.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    fio = table.Column<string>(maxLength: 50, nullable: true),
                    seriya = table.Column<int>(nullable: false),
                    nomer = table.Column<int>(nullable: false),
                    telefon = table.Column<string>(maxLength: 20, nullable: true),
                    otpechatok = table.Column<string>(maxLength: 100, nullable: true),
                    kemVidan = table.Column<string>(maxLength: 250, nullable: true),
                    dataVidachi = table.Column<DateTime>(nullable: false),
                    adresProj = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethod",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    description = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethod", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Placement",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    city = table.Column<string>(maxLength: 50, nullable: true),
                    street = table.Column<string>(maxLength: 50, nullable: true),
                    house = table.Column<string>(maxLength: 50, nullable: true),
                    korpus = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Placement", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Trainer",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    fio = table.Column<string>(maxLength: 50, nullable: true),
                    foto = table.Column<string>(maxLength: 100, nullable: true),
                    zaslugi = table.Column<string>(maxLength: 2500, nullable: true),
                    phone = table.Column<string>(maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trainer", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TypeCard",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    price = table.Column<int>(nullable: false),
                    dlitZam = table.Column<int>(nullable: false),
                    zamozka = table.Column<bool>(nullable: false),
                    dlitDeist = table.Column<int>(nullable: false),
                    description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeCard", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TypeOccupation",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeOccupation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    fio = table.Column<string>(maxLength: 50, nullable: true),
                    phone = table.Column<string>(maxLength: 20, nullable: true),
                    positionID = table.Column<int>(nullable: false),
                    login = table.Column<string>(maxLength: 50, nullable: true),
                    password = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.id);
                    table.ForeignKey(
                        name: "FK_Employee_Position_positionID",
                        column: x => x.positionID,
                        principalTable: "Position",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClubCard",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    clientID = table.Column<int>(nullable: false),
                    typecardID = table.Column<int>(nullable: false),
                    dateAktiv = table.Column<DateTime>(nullable: false),
                    dateVidach = table.Column<DateTime>(nullable: false),
                    dateOkonP = table.Column<DateTime>(nullable: false),
                    dateOkonF = table.Column<DateTime>(nullable: false),
                    dlitZam = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCard", x => x.id);
                    table.ForeignKey(
                        name: "FK_ClubCard_Client_clientID",
                        column: x => x.clientID,
                        principalTable: "Client",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClubCard_TypeCard_typecardID",
                        column: x => x.typecardID,
                        principalTable: "TypeCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Schedule",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    vrNach = table.Column<TimeSpan>(nullable: false),
                    vrOkon = table.Column<TimeSpan>(nullable: false),
                    typeoccupationID = table.Column<int>(nullable: false),
                    placementID = table.Column<int>(nullable: false),
                    triner_id = table.Column<int>(nullable: false),
                    data = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(maxLength: 250, nullable: true),
                    size = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedule", x => x.id);
                    table.ForeignKey(
                        name: "FK_Schedule_Placement_placementID",
                        column: x => x.placementID,
                        principalTable: "Placement",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Schedule_TypeOccupation_typeoccupationID",
                        column: x => x.typeoccupationID,
                        principalTable: "TypeOccupation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClubCardRoom",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    clubcardID = table.Column<int>(nullable: false),
                    placementID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCardRoom", x => x.id);
                    table.ForeignKey(
                        name: "FK_ClubCardRoom_ClubCard_clubcardID",
                        column: x => x.clubcardID,
                        principalTable: "ClubCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClubCardRoom_Placement_placementID",
                        column: x => x.placementID,
                        principalTable: "Placement",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractServices",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dataZakl = table.Column<DateTime>(nullable: false),
                    summa = table.Column<int>(nullable: false),
                    employeeID = table.Column<int>(nullable: false),
                    paymentmethodID = table.Column<int>(nullable: false),
                    clientID = table.Column<int>(nullable: false),
                    clubcardID = table.Column<int>(nullable: false),
                    typecardID = table.Column<int>(nullable: false),
                    placementID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractServices", x => x.id);
                    table.ForeignKey(
                        name: "FK_ContractServices_Client_clientID",
                        column: x => x.clientID,
                        principalTable: "Client",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractServices_ClubCard_clubcardID",
                        column: x => x.clubcardID,
                        principalTable: "ClubCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractServices_Employee_employeeID",
                        column: x => x.employeeID,
                        principalTable: "Employee",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractServices_PaymentMethod_paymentmethodID",
                        column: x => x.paymentmethodID,
                        principalTable: "PaymentMethod",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractServices_Placement_placementID",
                        column: x => x.placementID,
                        principalTable: "Placement",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractServices_TypeCard_typecardID",
                        column: x => x.typecardID,
                        principalTable: "TypeCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Visits",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    vremVh = table.Column<DateTime>(nullable: false),
                    vremVih = table.Column<DateTime>(nullable: false),
                    data = table.Column<DateTime>(nullable: false),
                    clubcardID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visits", x => x.id);
                    table.ForeignKey(
                        name: "FK_Visits_ClubCard_clubcardID",
                        column: x => x.clubcardID,
                        principalTable: "ClubCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClubCardSchedule",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    clubcardID = table.Column<int>(nullable: false),
                    scheduleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCardSchedule", x => x.id);
                    table.ForeignKey(
                        name: "FK_ClubCardSchedule_ClubCard_clubcardID",
                        column: x => x.clubcardID,
                        principalTable: "ClubCard",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClubCardSchedule_Schedule_scheduleID",
                        column: x => x.scheduleID,
                        principalTable: "Schedule",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClubCard_clientID",
                table: "ClubCard",
                column: "clientID");

            migrationBuilder.CreateIndex(
                name: "IX_ClubCard_typecardID",
                table: "ClubCard",
                column: "typecardID");

            migrationBuilder.CreateIndex(
                name: "IX_ClubCardRoom_clubcardID",
                table: "ClubCardRoom",
                column: "clubcardID");

            migrationBuilder.CreateIndex(
                name: "IX_ClubCardRoom_placementID",
                table: "ClubCardRoom",
                column: "placementID");

            migrationBuilder.CreateIndex(
                name: "IX_ClubCardSchedule_clubcardID",
                table: "ClubCardSchedule",
                column: "clubcardID");

            migrationBuilder.CreateIndex(
                name: "IX_ClubCardSchedule_scheduleID",
                table: "ClubCardSchedule",
                column: "scheduleID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_clientID",
                table: "ContractServices",
                column: "clientID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_clubcardID",
                table: "ContractServices",
                column: "clubcardID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_employeeID",
                table: "ContractServices",
                column: "employeeID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_paymentmethodID",
                table: "ContractServices",
                column: "paymentmethodID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_placementID",
                table: "ContractServices",
                column: "placementID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractServices_typecardID",
                table: "ContractServices",
                column: "typecardID");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_positionID",
                table: "Employee",
                column: "positionID");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_placementID",
                table: "Schedule",
                column: "placementID");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_typeoccupationID",
                table: "Schedule",
                column: "typeoccupationID");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_clubcardID",
                table: "Visits",
                column: "clubcardID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubCardRoom");

            migrationBuilder.DropTable(
                name: "ClubCardSchedule");

            migrationBuilder.DropTable(
                name: "ContractServices");

            migrationBuilder.DropTable(
                name: "Trainer");

            migrationBuilder.DropTable(
                name: "Visits");

            migrationBuilder.DropTable(
                name: "Schedule");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "PaymentMethod");

            migrationBuilder.DropTable(
                name: "ClubCard");

            migrationBuilder.DropTable(
                name: "Placement");

            migrationBuilder.DropTable(
                name: "TypeOccupation");

            migrationBuilder.DropTable(
                name: "Position");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropTable(
                name: "TypeCard");
        }
    }
}
