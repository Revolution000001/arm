﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TrainerController : Controller
    {
        private AppDBContent _db;
        private readonly ITrainer _trainer;

        public TrainerController(ITrainer trainer, AppDBContent db)
        {
            _trainer = trainer;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Trainer> getTrainers()
        {
            var trainers = _trainer.Trainers;
            return trainers;
        }

        [HttpGet("[action]")]
        public Trainer getTrainer([FromQuery]int id)
        {
            var trainer = _trainer.getTrainer(id);
            return trainer;
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> AddTrainer([FromBody]Trainer trainer)
        {
            _db.Trainer.Add(trainer);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditTrainer([FromBody]Trainer trainer)
        {

            var tr = _db.Trainer
                .Where(x => x.id == trainer.id)
                .FirstOrDefault();
            tr.fio = trainer.fio;
            tr.zaslugi = trainer.zaslugi;
            tr.phone = trainer.phone;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteTrainer([FromBody]Trainer trainer)
        {
            var tr = _db.Trainer
                .Where(x => x.id == trainer.id)
                .FirstOrDefault();
            _db.Trainer.Remove(tr);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
