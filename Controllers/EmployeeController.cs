﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        private AppDBContent _db;
        private readonly IEmployee _employee;

        public EmployeeController(IEmployee employee, AppDBContent db)
        {
            _employee = employee;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Employee> getEmployees()
        {
            var employees = _employee.Employees;
            return employees;
        }

        [HttpGet("[action]")]
        public Employee getEmpolyee([FromQuery]int id)
        {
            var employee = _employee.getEmployee(id);
            return employee;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddEmployee([FromBody]Employee employee)
        {
            Position position = _db.Position
                .FirstOrDefault(x => x.id == employee.positionId);

            Employee Employee = new Employee
            {
                fio = employee.fio,
                phone = employee.phone,
                Position = position,
                login = employee.login,
                password = employee.password,
            };
            _db.Employee.Add(Employee);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditEmployee([FromBody]Employee employee)
        {
            Position position = _db.Position
               .FirstOrDefault(x => x.id == employee.positionId);

            var emp = _db.Employee
                .Where(x => x.id == employee.id)
                .FirstOrDefault();
            emp.fio = employee.fio;
            emp.phone = employee.phone;
            emp.Position = position;
            emp.login = employee.login;
            emp.password = employee.password;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteEmployee([FromBody]Employee employee)
        {
            var emp = _db.Employee
                .Where(x => x.id == employee.id)
                .FirstOrDefault();
            _db.Employee.Remove(emp);
            await _db.SaveChangesAsync();
            return Ok();
        }
        //[HttpGet("[action]")]
        //public Task<IActionResult> getAddEmployees()
        //{
        //    var positions = _db.Position;
        //    return positions;
        //}
    }
}
