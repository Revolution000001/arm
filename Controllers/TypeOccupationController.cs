﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TypeOccupationController : Controller
    {
        private AppDBContent _db;
        private readonly ITypeOccupation _typeOccupation;

        public TypeOccupationController(ITypeOccupation typeOccupation, AppDBContent db)
        {
            _typeOccupation = typeOccupation;
             _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<TypeOccupation> getTypes()
        {
            var typeOccupations = _typeOccupation.TypeOccupations;
            return typeOccupations;
        }

        [HttpGet("[action]")]
        public TypeOccupation getType([FromQuery]int id)
        {
            var type = _typeOccupation.getTypeOccupation(id);
            return type;

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddType([FromBody]TypeOccupation type)
        {
            _db.TypeOccupation.Add(type);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditType([FromBody]TypeOccupation type)
        {

            var typeOc = _db.TypeOccupation
                .Where(x => x.id == type.id)
                .FirstOrDefault();
            typeOc.name = type.name;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteType([FromBody]TypeOccupation type)
        {
            var typeOc = _db.TypeOccupation
                .Where(x => x.id == type.id)
                .FirstOrDefault();
            _db.TypeOccupation.Remove(typeOc);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
