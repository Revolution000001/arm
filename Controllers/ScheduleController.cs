﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        private AppDBContent _db;
        private readonly ISchedule _schedule;

        public ScheduleController(ISchedule schedule, AppDBContent db)
        {
            _schedule = schedule;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Schedule> getSchedules()
        {
            var schedules = _schedule.Schedules;
            return schedules;
        }

        [HttpGet("[action]")]
        public Schedule getSchedule([FromQuery]int id)
        {
            var schedule = _schedule.getSchedule(id);
            return schedule;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddSchedule([FromBody]Schedule schedule)
        {
            Placement placement = _db.Placement
                .FirstOrDefault(x => x.id == schedule.placementID);
            Trainer trainer = _db.Trainer
                .FirstOrDefault(x => x.id == schedule.trainerID);
            TypeOccupation type = _db.TypeOccupation
                .FirstOrDefault(x => x.id == schedule.typeoccupationID);

            Schedule schedule_new = new Schedule
            {
                name = schedule.name,
                vrNach = schedule.vrNach,
                vrOkon = schedule.vrOkon,
                Placement = placement,
                Trainer = trainer,
                TypeOccupation = type,
                data = schedule.data,
                description = schedule.description,
                size = schedule.size,
            };
            _db.Schedule.Add(schedule_new);
            await _db.SaveChangesAsync();
            return Ok();
        }

        public class Schedule_Edit
        {
            public int id { get; set; }
            public string name { get; set; }
            public TimeSpan vrNach { get; set; }
            public TimeSpan vrOkon { get; set; }
            public DateTime data { get; set; }
            public string description { get; set; }
            public int size { get; set; }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditSchedule([FromBody]Schedule_Edit schedule_edit)
        {
            var schedule = _db.Schedule
                .Where(x => x.id == schedule_edit.id)
                .FirstOrDefault();
            schedule.name = schedule_edit.name;
            schedule.vrNach = schedule_edit.vrNach;
            schedule.vrOkon = schedule_edit.vrOkon;
            schedule.data = schedule_edit.data;
            schedule.description = schedule_edit.description;
            schedule.size = schedule_edit.size;

            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
