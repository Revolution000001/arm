﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using ARM.Data;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PositionController : Controller
    {
        private AppDBContent _db;
        private readonly IPosition _position;

        public PositionController(IPosition position, AppDBContent db)
        {
            _position = position;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Position> getPositions()
        {
            var positions = _position.Positions;
            return positions;

        }

        [HttpGet("[action]")]
        public Position getPosition([FromQuery]int id)
        {
            var positions = _position.getPosition(id);
            return positions;

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddPosition([FromBody]Position position)
        {
            _db.Position.Add(position);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditPosition([FromBody]Position position)
        {

            var pos = _db.Position
                .Where(x => x.id == position.id)
                .FirstOrDefault();
            pos.name = position.name;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeletePosition([FromBody]Position position)
        {
            var pos = _db.Position
                .Where(x => x.id == position.id)
                .FirstOrDefault();
            _db.Position.Remove(pos);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
