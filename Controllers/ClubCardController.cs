﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ClubCardController : Controller
    {
        private AppDBContent _db;
        private readonly IClubCard _card;

        public ClubCardController(IClubCard card, AppDBContent db)
        {
            _card = card;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<ClubCard> getCards()
        {
            var cards = _card.ClubCards;
            return cards;
        }

        [HttpGet("[action]")]
        public ClubCard getCard([FromQuery]int id)
        {
            var card = _card.getClubCard(id);
            return card;
        }

        public class Card
        {
            public int id { get; set; }
            public DateTime dateOkon { get; set; }
            public int dlitZam { get; set; }
            public int frozen { get; set; }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditCard([FromBody]Card card)
        {

            var club = _db.ClubCard
                .Where(x => x.id == card.id)
                .FirstOrDefault();
            club.dateOkon = card.dateOkon.AddDays(card.frozen);
            club.dlitZam = card.dlitZam;

            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
