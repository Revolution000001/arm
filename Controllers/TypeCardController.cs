﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TypeCardController : Controller
    {
        private AppDBContent _db;
        private readonly ITypeCard _typeCard;

        public TypeCardController(ITypeCard typeCard, AppDBContent db)
        {
            _typeCard = typeCard;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<TypeCard> getTypes()
        {
            var typeCards = _typeCard.TypeCards;
            return typeCards;
        }

        [HttpGet("[action]")]
        public TypeCard getType([FromQuery]int id)
        {
            var type = _typeCard.getTypeCard(id);
            return type;

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddType([FromBody]TypeCard type)
        {
            _db.TypeCard.Add(type);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditType([FromBody]TypeCard type)
        {

            var typeC = _db.TypeCard
                .Where(x => x.id == type.id)
                .FirstOrDefault();
            typeC.name = type.name;
            typeC.price = type.price;
            typeC.dlitZam = type.dlitZam;
            typeC.dlitDeist = type.dlitDeist;
            typeC.description = type.description;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteType([FromBody]TypeCard type)
        {
            var typeC = _db.TypeCard
                .Where(x => x.id == type.id)
                .FirstOrDefault();
            _db.TypeCard.Remove(typeC);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
