﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using FastMember;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ContractServicesController : Controller
    {
        private AppDBContent _db;
        private readonly IContractServices _contract;

        public ContractServicesController(IContractServices contract, AppDBContent db)
        {
            _contract = contract;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<ContractServices> getContracts()
        {
            var contractServices = _contract.ContractServices;
            return contractServices;
        }

        public class ContractService
        {
            public string fio { get; set; }
            public string phone { get; set; }
            public int seriya { get; set; }
            public int number { get; set; }
            public string kemVidan { get; set; }
            public DateTime dataVidachi { get; set; }
            public string adresProj { get; set; }
            public int typecardID { get; set; }
            public int employeeID { get; set; }
            public int placementID { get; set; }
            public int paymentmethodID { get; set; }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddContract([FromBody]ContractService contract)
        {
            Client client = new Client
            {
                fio = contract.fio,
                seriya = contract.seriya,
                number = contract.number,
                phone = contract.phone,
                kemVidan = contract.kemVidan,
                dataVidachi = contract.dataVidachi,
                adresProj = contract.adresProj
            };
            _db.Client.Add(client);
            await _db.SaveChangesAsync();

            PaymentMethod method = _db.PaymentMethod
                .FirstOrDefault(x => x.id == contract.paymentmethodID);

            Placement placement = _db.Placement
                .FirstOrDefault(x => x.id == contract.placementID);

            Employee employee = _db.Employee
                .FirstOrDefault(x => x.id == contract.employeeID);

            TypeCard type = _db.TypeCard
                .FirstOrDefault(x => x.id == contract.typecardID);

            ClubCard clubCard = new ClubCard
            {
                Client = client,
                TypeCard = type,
                Placement = placement,
                dateVidach = DateTime.Now,
                dateOkon = DateTime.Now.AddMonths(type.dlitDeist),
                dlitZam = type.dlitZam
            };
            _db.ClubCard.Add(clubCard);
            await _db.SaveChangesAsync();

            ContractServices contractServices = new ContractServices
            {
                dataZakl = DateTime.Now,
                summa = type.price,
                Employee = employee,
                PaymentMethod = method,
                Client = client,
                ClubCard = clubCard,
                TypeCard = type,
                Placement = placement,
            };
            _db.ContractServices.Add(contractServices);
            await _db.SaveChangesAsync();
            return Ok();
        }


        [HttpGet("[action]")]
        public IActionResult ExportExcel()
        {
            var contractServices = _contract.ContractServices.Select(x => new
            {
                Номер = x.id,
                Дата_Заключения = x.dataZakl.ToShortDateString(),
                Сумма = x.summa,
                Клиент = x.Client.fio,
                Тип_Карты = x.TypeCard.name,
                Помещение = x.Placement.name
            });

            DataTable dt = new DataTable();
            using(var reader = ObjectReader.Create(contractServices))
            {
                dt.Load(reader);
            }

            byte[] fileContents;
            using(var package = new ExcelPackage())
            {
                var workSheet = package.Workbook.Worksheets.Add("Продажа карт");
                workSheet.Cells["A1"].LoadFromDataTable(dt, true);
                fileContents = package.GetAsByteArray();
            }
            if(fileContents == null || fileContents.Length == 0)
            {
                return null;
            }
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName:"ПродажиКарт.xlsx"
                );
        }
    }
}
