﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using ARM.Data;
using Microsoft.AspNetCore.Authorization;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ClientController : Controller
    {
        private AppDBContent _db;
        private readonly IClient _client;

        public ClientController(IClient client, AppDBContent db)
        {
            _client = client;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Client> getClients()
        {
            var clients = _client.Clients;
            return clients;
        }

        [HttpGet("[action]")]
        public Client getClient([FromQuery]int id)
        {
            var client = _client.getClient(id);
            return client;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditClient([FromBody]Client client)
        {
            var cl = _db.Client
                .Where(x => x.id == client.id)
                .FirstOrDefault();
            cl.fio = client.fio;
            cl.seriya = client.seriya;
            cl.number = client.number;
            cl.phone = client.phone;
            cl.kemVidan = client.kemVidan;
            cl.dataVidachi = client.dataVidachi;
            cl.adresProj = client.adresProj;

            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
