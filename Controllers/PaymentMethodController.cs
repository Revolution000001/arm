﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentMethodController : Controller
    {
        private AppDBContent _db;
        private readonly IPaymentMethod _payment;

        public PaymentMethodController(IPaymentMethod payment, AppDBContent db)
        {
            _payment = payment;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<PaymentMethod> getMethods()
        {
            var payments = _payment.PaymentMethods;
            return payments;
        }

        [HttpGet("[action]")]
        public PaymentMethod getPayment([FromQuery]int id)
        {
            var payment = _payment.getPaymentMethod(id);
            return payment;

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddPayment([FromBody]PaymentMethod payment)
        {
            _db.PaymentMethod.Add(payment);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditPayment([FromBody]PaymentMethod payment)
        {

            var pay = _db.PaymentMethod
                .Where(x => x.id == payment.id)
                .FirstOrDefault();
            pay.name = payment.name;
            pay.description = payment.description;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeletePayment([FromBody]PaymentMethod payment)
        {
            var pay = _db.PaymentMethod
                .Where(x => x.id == payment.id)
                .FirstOrDefault();
            _db.PaymentMethod.Remove(pay);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
