﻿using ARM.Data;
using ARM.Data.Interfeces;
using ARM.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARM.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PlacementController : Controller
    {
        private AppDBContent _db;
        private readonly IPlacement _placement;

        public PlacementController(IPlacement placement, AppDBContent db)
        {
            _placement = placement;
            _db = db;
        }

        [HttpGet("[action]")]
        public IEnumerable<Placement> getPlacements()
        {
            var placements = _placement.Placements;
            return placements;
        }

        [HttpGet("[action]")]
        public Placement getPlacement([FromQuery]int id)
        {
            var placement = _placement.getPlacement(id);
            return placement;
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> AddPlacement([FromBody]Placement placement)
        {
            _db.Placement.Add(placement);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditPlacement([FromBody]Placement placement)
        {

            var plac = _db.Placement
                .Where(x => x.id == placement.id)
                .FirstOrDefault();
            plac.name = placement.name;
            plac.city = placement.city;
            plac.street = placement.street;
            plac.house = placement.house;
            plac.korpus = placement.korpus;

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeletePosition([FromBody]Placement placement)
        {
            var plac = _db.Placement
                .Where(x => x.id == placement.id)
                .FirstOrDefault();
            _db.Placement.Remove(plac);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
